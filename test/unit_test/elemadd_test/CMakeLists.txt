add_executable(arx_elemadd_test
        elemadd_test.c)


add_custom_command(
        TARGET arx_elemadd_test POST_BUILD
        COMMAND python3 ${CMAKE_HOME_DIRECTORY}/python/elemadd_input_gen.py 1024
)



#        USE_DIFF 1 DIFF_TARGET ${CMAKE_CURRENT_SOURCE_DIR}/vtaElemAddTestGolden.bin))
#add_test(NAME arx_elemadd_test
#        COMMAND arx_elemadd_test input1.bin input2.bin)

target_link_libraries(arx_elemadd_test PUBLIC arx_api)

#add_test(NAME arx_elemadd_test_compare
#        COMMAND diff golden.bin output.bin)
#set_property(GLOBAL APPEND PROPERTY ARXAPI_TEST_DEPENDS arx_elemadd_test)
#set_property(TEST arx_elemadd_test  PROPERTY LABELS TESTS)
#set_property(GLOBAL APPEND PROPERTY ARXAPI_TEST_DEPENDS arx_elemadd_test_compare)
#set_property(TEST arx_elemadd_test_compare  PROPERTY LABELS TESTS)
#set_property(TEST arx_elemadd_test_compare APPEND PROPERTY DEPENDS arx_elemadd_test)
add_arxapi_test(NAME arx_elemadd_test COMMAND ${CMAKE_CURRENT_BINARY_DIR}/arx_elemadd_test USE_SH 1 PARAMS "${CMAKE_CURRENT_BINARY_DIR}/input1.bin ${CMAKE_CURRENT_BINARY_DIR}/input2.bin"
        USE_DIFF 1 DIFF_TARGET ${CMAKE_CURRENT_BINARY_DIR}/golden.bin)


add_subdirectory(Unittests)