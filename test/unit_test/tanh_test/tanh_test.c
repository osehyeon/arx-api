//
// Created by User on 2024-01-10.
//
#include "include/API.h"
#include "include/ONES_MATH.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char **argv) {
    FILE *fp;
    unsigned char *input0;
    long size;
    // read arguments
    if (argc == 2) {
        // argv[1] is the input0 file
        fp = fopen(argv[1], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        input0 = malloc(fsize);
        fread(input0, fsize, 1, fp);
        fclose(fp);

    }
    else if (argc != 1) {
        printf("Invalid number of arguments\n");
    }

    else {
        // data size = 1024
        size = 1024;
        input0 = malloc(size);

        // initialize input0 and input1 with random data
        for (int i = 0; i < 1024; i++) {
            input0[i] = (char) rand();
            printf("%d ", input0[i]);
        }
    }
    printf("\n");

    // float *output = malloc(4*size);
    unsigned char * output = malloc(size);
    for(int i = 0; i < size; i++) {
        printf("%lf ", ones_tanh((float)input0[i]/100.0));
        // printf("%lf ", ones_tanh(input0[i]));
    }
    printf("\n");
    printf("%lf\n", ones_tanh(0.099833));
// input_data[0]: 0.000000
// input_data[1]: 0.099833
// input_data[2]: 0.198669
// input_data[3]: 0.295520
// input_data[4]: 0.389418
// input_data[5]: 0.479426
// input_data[6]: 0.564642
// input_data[7]: 0.644218
// input_data[8]: 0.717356
// input_data[9]: 0.783327
    // float *test_input=malloc(1);
    // test_input[0]=0.295520;
    // unsigned char* quantized_input=malloc(1);
    // quantized_input[0]=(unsigned char)ones_round(test_input[0]*100+128);

    // printf("%u\n", quantized_input[0]);

    // unsigned char *test_output= malloc(1);
    // tanh_activation_i8(quantized_input,test_output,1,(float[]){0.01}, (unsigned char[]){128.0});
    // printf("%u\n", test_output[0]);

    // float* output_float = malloc(4);
    // output_float[0] = (float)ones_round(test_output[0] - 128) * 0.01;
    // printf("%f\n", output_float[0]);
    // printf("%f\n", ones_tanh(0.295520));
    // input 이 167이면 dequantize 하면 0.39가 나와야함
    // 0.39를 tanh에 넣으면 0.371360가 나와야함
    // 그거를 또 quantize하면 165가 나와야함
    // scale = 0.01, offset = 128 이기 때문이다. 현재

    float input0_scale[] = {0.01}; unsigned char input0_offset[] = {128.0};
    tanh_activation_i8(input0, output, size, input0_scale,input0_offset);
    for(int i = 0; i < size; i++) {
        printf("%u ", output[i]);
    }
    // write output to file

    // unsigned char *output_int8 = malloc(size);
    // float input0_scale_i8[] = {0.02}; unsigned char input0_offset_i8[] = {128.0};
    // tanh_activation_i8(input0, output_int8, size, input0_scale_i8, input0_offset_i8);
    // for(int i = 0; i < size; i++) {
    //     printf("%d ", output_int8[i]);
    // }

// output 배열 소수점 4자리 이하에서 버림
    // for (long i = 0; i < size; i++) {
    //     output[i] = ones_floorf(output[i] * 1e6) / 1e6;
    // }

    fp = fopen("output.bin", "wb");
    fwrite(output, size, 1, fp);
    fclose(fp);
    free(output);

    return 0;
}
