#ifdef RUN_ON_HOST_COMP

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "../build/include/vocab.h"
#include "../build/include/small_embedding.h"
#include "../build/include/small_lstm_weights.h"
#include "../build/include/small_dense_weights.h"

#else

#include <stdint.h>
#include "platform_info.h"
#include "ervp_printf.h"
#include "ervp_malloc.h"
#include "ervp_multicore_synch.h"
#include "../include/vocab.h"
#include "../include/small_embedding.h"
#include "../include/small_lstm_weights.h"
#include "../include/small_dense_weights.h"

#endif




#define VOCAB_SIZE 65  // 예시
#define EMBEDDING_DIM 64
#define RNN_UNITS 64
#define BATCH_SIZE 1
#define SEQUENCE_LEN 1
#define INPUT_TEXT_LEN 1

// 행렬 곱셈 함수
// matmul(&temp1[0][0], &x_t[0][time_stamp], &W_i[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
void make_dict(char *idx2char, char*char2idx, char* vocab){
    for(int i = 0; i < VOCAB_SIZE; i++){
        idx2char[i] = vocab[i];
        char2idx[vocab[i]] = i;
    }
}
float ones_exp(float x, int terms) {

    float result = 1.0f;
    float term = 1.0f;

    if (x == 0.0f) {
        return 1.0f; // 입력이 0인 경우 예외 처리
    }
    if (x < 0.0f) {
        // 입력이 음수인 경우 양수로 변환
        x = -x;
    }
    for (int i = 1; i < terms; i++) {
        term *= x / (float)i;
        result += term;
    }
    return 1.0f / result;
}
float ones_mod_exp(float x, int terms) {
    float result = 1.0f;
    float term = 1.0f;

    // 처리하기 쉽도록 x를 양수로 만듭니다.
    float abs_x = (x < 0.0f) ? -x : x;

    if (x == 0.0f) {
        return 1.0f; // e^0은 1입니다.
    }

    // Taylor 시리즈를 이용하여 e^x 근사
    for (int i = 1; i < terms; ++i) {
        term *= abs_x / (float)i;
        result += term;
    }

    // 입력이 음수일 경우 e^(-x)의 역수를 반환
    if (x < 0.0f) {
        return 1.0f / result;
    }

    return result;
}
float ones_tanh(float x){
    return (ones_mod_exp(2*x,64)-1)/(ones_mod_exp(2*x,64)+1);
}
float ones_round(float number) {
    int intPart = (int)number; 
    float fracPart = number - intPart; 

    if (fracPart == 0.5 || fracPart == -0.5) {
        if (intPart % 2 == 0) {
            return intPart;
        }
        return (number > 0) ? intPart + 1 : intPart - 1;
    }
    
    if (fracPart >= 0.5 || fracPart <= -0.5) {
        return (number > 0) ? intPart + 1 : intPart - 1;
    } else {
        return intPart;
    }
}

float ones_floorf(float x) {
    if (x >= 0) {
        return (int)x;  // 양의 실수의 경우, 정수 부분만 반환
    } else {
        int ix = (int)x;  // 음의 실수의 경우, 더 작은 정수로 내림
        return (float)(x == ix ? ix : ix - 1);
    }
}
// 시그모이드 함수
float sigmoid(float x) {
    return 1.0f / (1.0f + ones_mod_exp(-x,64));
}

// 시그모이드 적용 함수
void apply_sigmoid(float* result, int size) {
    for (int i = 0; i < size; i++) {
        result[i] = sigmoid(result[i]);
    }
}

// tanh 함수
void apply_tanh(float* result, int size) {
    for (int i = 0; i < size; i++) {
        result[i] = ones_tanh(result[i]);
    }
}
void matmul(float* result, float* mat1, float* mat2, int rows1, int cols1, int cols2) {
    for(int i = 0; i < rows1; i++) {
        for(int j = 0; j < cols2; j++) {
            result[i * cols2 + j] = 0.0;
            for(int k = 0; k < cols1; k++) {
                result[i * cols2 + j] += mat1[i * cols1 + k] * mat2[k * cols2 + j];
            }
        }
    }
}

// 벡터 더하기 함수
void add_bias(float* result, float* bias, int size) {
    for (int i = 0; i < size; i++) {
        result[i] += bias[i];
    }
}
// 바이어스 더하기 함수 (출력에 바이어스를 더합니다)
void add_bias1(float* result, float* bias, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            result[i * cols + j] += bias[j];
        }
    }
}

void lstm_layer_calculate_input_gate(int time_stamp,float input_gate[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS], float W_i[EMBEDDING_DIM][RNN_UNITS], float U_i[RNN_UNITS][RNN_UNITS], float b_i[RNN_UNITS]){
    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0][0], &W_i[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_i[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);
    
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            input_gate[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&input_gate[b][0], b_i, RNN_UNITS);
        apply_sigmoid(&input_gate[b][0], RNN_UNITS);
    }
}
void lstm_layer_calculate_forget_gate(int time_stamp,float forget_gate[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS], float W_f[EMBEDDING_DIM][RNN_UNITS], float U_f[RNN_UNITS][RNN_UNITS], float b_f[RNN_UNITS]){
    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0][0], &W_f[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_f[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);
    
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            forget_gate[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&forget_gate[b][0], b_f, RNN_UNITS);
        apply_sigmoid(&forget_gate[b][0], RNN_UNITS);
    }
}
// 후보 셀 상태 계산
void lstm_layer_calculate_candidate_cell_state(int time_stamp,float candidate_cell_state[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS],
    float W_C[EMBEDDING_DIM][RNN_UNITS], float U_C[RNN_UNITS][RNN_UNITS], float b_C[RNN_UNITS]) {

    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0], &W_C[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_C[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            candidate_cell_state[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&candidate_cell_state[b][0], b_C, RNN_UNITS);
        apply_tanh(&candidate_cell_state[b][0], RNN_UNITS);
    }
}
void lstm_layer_calculate_output_gate(int time_stamp,float output_gate[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS], float W_o[EMBEDDING_DIM][RNN_UNITS], float U_o[RNN_UNITS][RNN_UNITS], float b_o[RNN_UNITS]){
    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0][0], &W_o[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_o[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);
    
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            output_gate[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&output_gate[b][0], b_o, RNN_UNITS);
        apply_sigmoid(&output_gate[b][0], RNN_UNITS);
    }
}
// 셀 상태 계산
void lstm_layer_calculate_cell_state(float cell_state[BATCH_SIZE][RNN_UNITS], float C_prev[BATCH_SIZE][RNN_UNITS], float forget_gate[BATCH_SIZE][RNN_UNITS], 
    float input_gate[BATCH_SIZE][RNN_UNITS], float candidate_cell_state[BATCH_SIZE][RNN_UNITS]) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            cell_state[b][r] = forget_gate[b][r] * C_prev[b][r] + input_gate[b][r] * candidate_cell_state[b][r];
        }
    }
}

// 은닉 상태 계산
void lstm_layer_calculate_hidden_state(float hidden_state[BATCH_SIZE][RNN_UNITS], float cell_state[BATCH_SIZE][RNN_UNITS], float output_gate[BATCH_SIZE][RNN_UNITS]) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            float tanh = ones_tanh(cell_state[b][r]);
            hidden_state[b][r] = output_gate[b][r] * tanh;
        }
    }
}

// 상태 업데이트
void lstm_layer_update_states(int timestep, float h_list[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS], float h_prev[BATCH_SIZE][RNN_UNITS],
    float C_prev[BATCH_SIZE][RNN_UNITS], float hidden_state[BATCH_SIZE][RNN_UNITS], float cell_state[BATCH_SIZE][RNN_UNITS]) {

    for(int b = 0; b < BATCH_SIZE; b++) {
        for(int r = 0; r < RNN_UNITS; r++) {
            h_prev[b][r] = hidden_state[b][r];
            C_prev[b][r] = cell_state[b][r];
            h_list[b][timestep][r] = hidden_state[b][r];
        }
    }
}
void lstm_layer_forward(float h_list[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS], float h_prev[BATCH_SIZE][RNN_UNITS],float C_prev[BATCH_SIZE][RNN_UNITS],
    float W_i[EMBEDDING_DIM][RNN_UNITS],float W_f[EMBEDDING_DIM][RNN_UNITS],float W_C[EMBEDDING_DIM][RNN_UNITS],float W_o[EMBEDDING_DIM][RNN_UNITS],
    float U_i[RNN_UNITS][RNN_UNITS],float U_f[RNN_UNITS][RNN_UNITS],float U_C[RNN_UNITS][RNN_UNITS],float U_o[RNN_UNITS][RNN_UNITS],
    float b_i[RNN_UNITS],float b_f[RNN_UNITS],float b_C[RNN_UNITS],float b_o[RNN_UNITS]){

    float input_gate[BATCH_SIZE][RNN_UNITS], forget_gate[BATCH_SIZE][RNN_UNITS], candidate_cell_state[BATCH_SIZE][RNN_UNITS], cell_state[BATCH_SIZE][RNN_UNITS], output_gate[BATCH_SIZE][RNN_UNITS], hidden_state[BATCH_SIZE][RNN_UNITS];

    for(int i=0;i<SEQUENCE_LEN;i++){
        lstm_layer_calculate_input_gate(i,input_gate,x_t,h_prev,W_i,U_i,b_i);
        lstm_layer_calculate_forget_gate(i,forget_gate,x_t,h_prev,W_f,U_f,b_f);
        lstm_layer_calculate_candidate_cell_state(i,candidate_cell_state,(float (*)[EMBEDDING_DIM])x_t,h_prev,W_C,U_C,b_C);
        lstm_layer_calculate_output_gate(i,output_gate,x_t,h_prev,W_o,U_o,b_o);
        lstm_layer_calculate_cell_state(cell_state,C_prev,forget_gate,input_gate,candidate_cell_state);
        lstm_layer_calculate_hidden_state(hidden_state,cell_state,output_gate);
        lstm_layer_update_states(i,h_list,h_prev,C_prev,hidden_state,cell_state);
    }
}

int main() {

    //1. python code 를 통해 저장된 idx2char.bin 파일을 읽어옵니다.
    // idx2char, char2idx 를 통해서 dictionary 만들기

    // char2idx = {u: i for i, u in enumerate(vocab)}
    // idx2char = np.array(vocab)
    // idx2char_for_save = np.array([ord(c) for c in vocab],dtype=np.uint8)
    // 0~65에 해당하는 index에 대해서 문자를 저장하는 배열 -> idx2char[0] = 'a' 이런식으로 사용
    char idx2char[VOCAB_SIZE];
    // 해당 배열의 문자에 대한 index를 저장하는 배열 -> char2idx['a'] = 0 이런식으로 사용, 얘는 0~255까지의 index를 가짐 
    // 실제로 사용하는 갯수는 65개
    char char2idx[255];
    make_dict(idx2char,char2idx,vocab);

    // 2. LSTM layer


    // 3. Dense layer
    

    // 4. Load the input text
    // char* input_text_string="ROMEO: ";
    const char start_text[2]="a";
    char input_text[2];
    input_text[0]='a';
    input_text[1]='\0';

    int n=1;
    char generated_text[102];

    float h_t[BATCH_SIZE][RNN_UNITS];
    float c_t[BATCH_SIZE][RNN_UNITS];
    // 초기 상태 초기화
    for(int i=0;i<RNN_UNITS;i++){
        h_t[0][i] = 0;
        c_t[0][i] = 0;
    }

    for(int i=0;i<10;i++){
        // 5. Forward pass

        // 5.1. Embedding layer
        float x_t[BATCH_SIZE][INPUT_TEXT_LEN][EMBEDDING_DIM];
        for(int i=0;i<INPUT_TEXT_LEN;i++){
            for(int j=0;j<EMBEDDING_DIM;j++){
                x_t[0][i][j] = embedding_vector[char2idx[input_text[i]]][j];
            }
        }

        // 5.2. LSTM layer
        float lstm_output[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS];
        lstm_layer_forward(lstm_output,x_t,h_t,c_t,W_i,W_f,W_C,W_o,U_i,U_f,U_C,U_o,b_i,b_f,b_C,b_o);

        // 5.3. Dense layer
        float output[BATCH_SIZE][SEQUENCE_LEN][VOCAB_SIZE];
            // Dense 레이어 계산
        for (int b = 0; b < BATCH_SIZE; b++) {
            for (int s = 0; s < SEQUENCE_LEN; s++) {
                matmul(&output[b][s][0], &lstm_output[b][s][0], &Dense_Wout[0][0], 1, RNN_UNITS, VOCAB_SIZE);
                add_bias1(&output[b][s][0], Dense_bout, 1, VOCAB_SIZE);
            }
        } 
        
        // 결과 출력

        int max_idx=0;
        float max_val = output[0][0][0];

        for (int b = 0; b < BATCH_SIZE; b++) {
            for (int s = 0; s < SEQUENCE_LEN; s++) {
                for (int v = 0; v < VOCAB_SIZE; v++) {
                    if(output[b][s][v]>max_val){
                        max_val = output[b][s][v];
                        max_idx = v;
                    }
                }
            }
        }
  
        input_text[0]=idx2char[max_idx];   
        generated_text[n++]=idx2char[max_idx]; 
    }
    generated_text[0]=start_text[0];
    for(int i=0;i<11;i++){
        printf("%c",generated_text[i]);
    }


    return 0;
}

