import tensorflow as tf
import numpy as np
from tqdm import tqdm

# 시드 설정
# np.random.seed(42)
# tf.random.set_seed(42)
# 문자 인코딩 매핑 로드
vocab = sorted(set(open(tf.keras.utils.get_file('shakespeare.txt', 'https://storage.googleapis.com/download.tensorflow.org/data/shakespeare.txt'), 'rb').read().decode(encoding='utf-8')))
char2idx = {u:i for i, u in enumerate(vocab)}
idx2char = np.array(vocab)
vocab_size = len(vocab)
embedding_dim = 256
rnn_units = 1024

def generate_text(model, start_string):
    num_generate = 50  # 생성할 문자의 수

    input_eval = [char2idx[s] for s in start_string] # 시작 문자열을 숫자로 변환
    input_eval = tf.expand_dims(input_eval, 0) # 배치 차원 추가

    text_generated = []


    # tqdm을 사용하여 로딩 바 추가
    for i in tqdm(range(num_generate), desc="Generating text"):
        predictions = model(input_eval)
        # print(predictions)
        # print(predictions.shape)
        predictions = tf.squeeze(predictions, 0)

        predicted_id = tf.argmax(predictions, axis=-1)[-1].numpy()

        input_eval = tf.expand_dims([predicted_id], 0)

        text_generated.append(idx2char[predicted_id])

    return (start_string + ''.join(text_generated))

# 저장된 모델 로드
model = tf.keras.models.load_model('./charRNN.h5')

gen_tx = generate_text(model, start_string="a")

print(gen_tx)

# 파일에 배열을 저장
def save_array_to_file(filename, data):
    data.tofile(filename)
    
array=np.array([ord(c) for c in gen_tx],dtype=np.uint8)
save_array_to_file("./text/golden_gen_text_by_model.bin",array)