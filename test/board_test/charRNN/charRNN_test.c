#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "include/ONES_MATH.h"
#define VOCAB_SIZE 65  // 예시
#define EMBEDDING_DIM 256
#define RNN_UNITS 256
#define BATCH_SIZE 1
#define SEQUENCE_LEN 1

#define DICTIONARY_PATH "./text/idx2char.bin"

#define EMBEDDING_WEIGHTS_PATH "./weights/embedding_weight_0.bin"
#define LSTM_KERNEL_WEIGHTS_PATH "./weights/lstm_weight_0.bin"
#define LSTM_RECURRENT_WEIGHTS_PATH "./weights/lstm_weight_1.bin"
#define LSTM_BIAS_PATH "./weights/lstm_weight_2.bin"
#define DENSE_WEIGHTS_PATH "./weights/dense_weight_0.bin"
#define DENSE_BIAS_PATH "./weights/dense_weight_1.bin"

// 행렬 곱셈 함수
// matmul(&temp1[0][0], &x_t[0][time_stamp], &W_i[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
void matmul(float* result, float* mat1, float* mat2, int rows1, int cols1, int cols2) {
    for(int i = 0; i < rows1; i++) {
        for(int j = 0; j < cols2; j++) {
            result[i * cols2 + j] = 0.0;
            for(int k = 0; k < cols1; k++) {
                result[i * cols2 + j] += mat1[i * cols1 + k] * mat2[k * cols2 + j];
            }
        }
    }
}

// 벡터 더하기 함수
void add_bias(float* result, float* bias, int size) {
    for (int i = 0; i < size; i++) {
        result[i] += bias[i];
    }
}
// 바이어스 더하기 함수 (출력에 바이어스를 더합니다)
void add_bias1(float* result, float* bias, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            result[i * cols + j] += bias[j];
        }
    }
}

// 시그모이드 함수
float sigmoid(float x) {
    return 1.0f / (1.0f + ones_mod_exp(-x,64));
}

// 시그모이드 적용 함수
void apply_sigmoid(float* result, int size) {
    for (int i = 0; i < size; i++) {
        result[i] = sigmoid(result[i]);
    }
}

// tanh 함수
void apply_tanh(float* result, int size) {
    for (int i = 0; i < size; i++) {
        result[i] = ones_tanh(result[i]);
    }
}
void save_string_to_binary_file(const char* filename, const char* str) {
    FILE* file = fopen(filename, "wb");  // 바이너리 쓰기 모드로 파일 열기
    if (file == NULL) {
        perror("Failed to open file for writing");
        return;
    }

    // 문자열을 바이너리 파일에 쓰기 (null-terminator 포함)
    size_t length = strlen(str);  // null-terminator 포함
    size_t written = fwrite(str, sizeof(char), length, file);
    if (written != length) {
        perror("Failed to write complete string to file");
    }

    fclose(file);  // 파일 닫기
}

void read_string_from_binary_file(const char* filename, char* buffer, size_t buffer_size) {
    FILE* file = fopen(filename, "rb");  // 바이너리 읽기 모드로 파일 열기
    if (file == NULL) {
        perror("Failed to open file for reading");
        return;
    }

    // 파일에서 문자열 읽기
    size_t read = fread(buffer, sizeof(char), buffer_size - 1, file);
    buffer[read] = '\0';  // 문자열 끝에 null-terminator 추가

    fclose(file);  // 파일 닫기
}
void print_array(int* data, size_t size) {
    for (size_t i = 0; i < size; i++) {
        printf("%d ", data[i]);
    }
    printf("\n");
}
// Function to load 32-bit float array from a file
char* load_8bit_int_array_from_file(const char *filename, size_t *size) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror("Error opening file");
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    // printf("file_size: %ld\n",file_size);
    fseek(fp, 0, SEEK_SET);

    *size = file_size / sizeof(char);
    char *array = (char *)malloc(file_size);
    if (array == NULL) {
        perror("Memory allocation failed");
        fclose(fp);
        return NULL;
    }

    fread(array, sizeof(char), *size, fp);
    fclose(fp);
    return array;
}
// Function to load 32-bit float array from a file
float* load_32bit_float_array_from_file(const char *filename, size_t *size) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror("Error opening file");
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *size = file_size / sizeof(float);
    float *array = (float *)malloc(file_size);
    if (array == NULL) {
        perror("Memory allocation failed");
        fclose(fp);
        return NULL;
    }

    fread(array, sizeof(float), *size, fp);
    fclose(fp);
    return array;
}


void make_dict(char *idx2char, char*char2idx, char* vocab){
    for(int i = 0; i < VOCAB_SIZE; i++){
        idx2char[i] = vocab[i];
        char2idx[vocab[i]] = i;
    }
}
void load_embedding_weights(float embedding_vector[VOCAB_SIZE][EMBEDDING_DIM],float *embedding){
    for(int i = 0; i < VOCAB_SIZE; i++){
        for(int j = 0; j < EMBEDDING_DIM; j++){
            embedding_vector[i][j] = embedding[i*EMBEDDING_DIM + j];
        }
    }
}
void load_lstm_weights(float W_i[EMBEDDING_DIM][RNN_UNITS],float W_f[EMBEDDING_DIM][RNN_UNITS],float W_C[EMBEDDING_DIM][RNN_UNITS],float W_o[EMBEDDING_DIM][RNN_UNITS],
                        float U_i[RNN_UNITS][RNN_UNITS],float U_f[RNN_UNITS][RNN_UNITS],float U_C[RNN_UNITS][RNN_UNITS],float U_o[RNN_UNITS][RNN_UNITS],
                        float b_i[RNN_UNITS],float b_f[RNN_UNITS],float b_C[RNN_UNITS],float b_o[RNN_UNITS],
                        float *kernel, float *recurrent_kernel, float *bias){
    float reshape_kernel[EMBEDDING_DIM][4*RNN_UNITS];
    float reshape_recurrent_kernel[RNN_UNITS][4*RNN_UNITS];
    float reshape_bias[4*RNN_UNITS];
    for(int i = 0; i < EMBEDDING_DIM; i++){
        for(int j = 0; j < 4*RNN_UNITS; j++){
            reshape_kernel[i][j] = kernel[i*4*RNN_UNITS + j];
        }
    }
    for(int i = 0; i < RNN_UNITS; i++){
        for(int j = 0; j < 4*RNN_UNITS; j++){
            reshape_recurrent_kernel[i][j] = recurrent_kernel[i*4*RNN_UNITS + j];
        }
    }
    for(int i = 0; i < 4*RNN_UNITS; i++){
        reshape_bias[i] = bias[i];
    }
    for(int i = 0; i < EMBEDDING_DIM; i++){
        for(int j = 0; j < RNN_UNITS; j++){
            W_i[i][j] = reshape_kernel[i][j];
            W_f[i][j] = reshape_kernel[i][RNN_UNITS + j];
            W_C[i][j] = reshape_kernel[i][2*RNN_UNITS + j];
            W_o[i][j] = reshape_kernel[i][3*RNN_UNITS + j];
        }
    }
    for(int i = 0; i < RNN_UNITS; i++){
        for(int j = 0; j < RNN_UNITS; j++){
            U_i[i][j] = reshape_recurrent_kernel[i][j];
            U_f[i][j] = reshape_recurrent_kernel[i][RNN_UNITS + j];
            U_C[i][j] = reshape_recurrent_kernel[i][2*RNN_UNITS + j];
            U_o[i][j] = reshape_recurrent_kernel[i][3*RNN_UNITS + j];
        }
    }
    for(int i = 0; i < RNN_UNITS; i++){
        b_i[i] = reshape_bias[i];
        b_f[i] = reshape_bias[RNN_UNITS + i];
        b_C[i] = reshape_bias[2*RNN_UNITS + i];
        b_o[i] = reshape_bias[3*RNN_UNITS + i];
    }

}

void load_dense_weights(float Dense_Wout[RNN_UNITS][VOCAB_SIZE],float Dense_bout[VOCAB_SIZE],float *Wout, float *bout){
    for(int i = 0; i < RNN_UNITS; i++){
        for(int j = 0; j < VOCAB_SIZE; j++){
            Dense_Wout[i][j] = Wout[i*VOCAB_SIZE + j];
        }
    }
    for(int i = 0; i < VOCAB_SIZE; i++){
        Dense_bout[i] = bout[i];
    }
}
void lstm_layer_calculate_input_gate(int time_stamp,float input_gate[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS], float W_i[EMBEDDING_DIM][RNN_UNITS], float U_i[RNN_UNITS][RNN_UNITS], float b_i[RNN_UNITS]){
    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0][0], &W_i[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_i[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);
    
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            input_gate[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&input_gate[b][0], b_i, RNN_UNITS);
        apply_sigmoid(&input_gate[b][0], RNN_UNITS);
    }
}
void lstm_layer_calculate_forget_gate(int time_stamp,float forget_gate[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS], float W_f[EMBEDDING_DIM][RNN_UNITS], float U_f[RNN_UNITS][RNN_UNITS], float b_f[RNN_UNITS]){
    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0][0], &W_f[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_f[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);
    
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            forget_gate[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&forget_gate[b][0], b_f, RNN_UNITS);
        apply_sigmoid(&forget_gate[b][0], RNN_UNITS);
    }
}
// 후보 셀 상태 계산
void lstm_layer_calculate_candidate_cell_state(int time_stamp,float candidate_cell_state[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS],
    float W_C[EMBEDDING_DIM][RNN_UNITS], float U_C[RNN_UNITS][RNN_UNITS], float b_C[RNN_UNITS]) {

    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0], &W_C[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_C[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            candidate_cell_state[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&candidate_cell_state[b][0], b_C, RNN_UNITS);
        apply_tanh(&candidate_cell_state[b][0], RNN_UNITS);
    }
}
void lstm_layer_calculate_output_gate(int time_stamp,float output_gate[BATCH_SIZE][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][EMBEDDING_DIM], float h_prev[BATCH_SIZE][RNN_UNITS], float W_o[EMBEDDING_DIM][RNN_UNITS], float U_o[RNN_UNITS][RNN_UNITS], float b_o[RNN_UNITS]){
    float temp1[BATCH_SIZE][RNN_UNITS];
    float temp2[BATCH_SIZE][RNN_UNITS];

    matmul(&temp1[0][0], &x_t[0][0][0], &W_o[0][0], BATCH_SIZE, EMBEDDING_DIM, RNN_UNITS);
    matmul(&temp2[0][0], &h_prev[0][0], &U_o[0][0], BATCH_SIZE, RNN_UNITS, RNN_UNITS);
    
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            output_gate[b][r] = temp1[b][r] + temp2[b][r];
        }
        add_bias(&output_gate[b][0], b_o, RNN_UNITS);
        apply_sigmoid(&output_gate[b][0], RNN_UNITS);
    }
}
// 셀 상태 계산
void lstm_layer_calculate_cell_state(float cell_state[BATCH_SIZE][RNN_UNITS], float C_prev[BATCH_SIZE][RNN_UNITS], float forget_gate[BATCH_SIZE][RNN_UNITS], 
    float input_gate[BATCH_SIZE][RNN_UNITS], float candidate_cell_state[BATCH_SIZE][RNN_UNITS]) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            cell_state[b][r] = forget_gate[b][r] * C_prev[b][r] + input_gate[b][r] * candidate_cell_state[b][r];
        }
    }
}

// 은닉 상태 계산
void lstm_layer_calculate_hidden_state(float hidden_state[BATCH_SIZE][RNN_UNITS], float cell_state[BATCH_SIZE][RNN_UNITS], float output_gate[BATCH_SIZE][RNN_UNITS]) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int r = 0; r < RNN_UNITS; r++) {
            float tanh = ones_tanh(cell_state[b][r]);
            hidden_state[b][r] = output_gate[b][r] * tanh;
        }
    }
}

// 상태 업데이트
void lstm_layer_update_states(int timestep, float h_list[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS], float h_prev[BATCH_SIZE][RNN_UNITS],
    float C_prev[BATCH_SIZE][RNN_UNITS], float hidden_state[BATCH_SIZE][RNN_UNITS], float cell_state[BATCH_SIZE][RNN_UNITS]) {

    for(int b = 0; b < BATCH_SIZE; b++) {
        for(int r = 0; r < RNN_UNITS; r++) {
            h_prev[b][r] = hidden_state[b][r];
            C_prev[b][r] = cell_state[b][r];
            h_list[b][timestep][r] = hidden_state[b][r];
        }
    }
}
void lstm_layer_forward(float h_list[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS], float x_t[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS], float h_prev[BATCH_SIZE][RNN_UNITS],float C_prev[BATCH_SIZE][RNN_UNITS],
    float W_i[EMBEDDING_DIM][RNN_UNITS],float W_f[EMBEDDING_DIM][RNN_UNITS],float W_C[EMBEDDING_DIM][RNN_UNITS],float W_o[EMBEDDING_DIM][RNN_UNITS],
    float U_i[RNN_UNITS][RNN_UNITS],float U_f[RNN_UNITS][RNN_UNITS],float U_C[RNN_UNITS][RNN_UNITS],float U_o[RNN_UNITS][RNN_UNITS],
    float b_i[RNN_UNITS],float b_f[RNN_UNITS],float b_C[RNN_UNITS],float b_o[RNN_UNITS]){

    float input_gate[BATCH_SIZE][RNN_UNITS], forget_gate[BATCH_SIZE][RNN_UNITS], candidate_cell_state[BATCH_SIZE][RNN_UNITS], cell_state[BATCH_SIZE][RNN_UNITS], output_gate[BATCH_SIZE][RNN_UNITS], hidden_state[BATCH_SIZE][RNN_UNITS];

    for(int i=0;i<SEQUENCE_LEN;i++){
        lstm_layer_calculate_input_gate(i,input_gate,x_t,h_prev,W_i,U_i,b_i);
        lstm_layer_calculate_forget_gate(i,forget_gate,x_t,h_prev,W_f,U_f,b_f);
        lstm_layer_calculate_candidate_cell_state(i,candidate_cell_state,(float (*)[EMBEDDING_DIM])x_t,h_prev,W_C,U_C,b_C);
        lstm_layer_calculate_output_gate(i,output_gate,x_t,h_prev,W_o,U_o,b_o);
        lstm_layer_calculate_cell_state(cell_state,C_prev,forget_gate,input_gate,candidate_cell_state);
        lstm_layer_calculate_hidden_state(hidden_state,cell_state,output_gate);
        lstm_layer_update_states(i,h_list,h_prev,C_prev,hidden_state,cell_state);
    }
}
int main() {

    // Load the embedding matrix

    //1. python code 를 통해 저장된 idx2char.bin 파일을 읽어옵니다.
    // idx2char, char2idx 를 통해서 dictionary 만들기

    // char2idx = {u: i for i, u in enumerate(vocab)}
    // idx2char = np.array(vocab)
    // idx2char_for_save = np.array([ord(c) for c in vocab],dtype=np.uint8)
    printf("charRNN test\n");

    size_t dict_size;
    char * vocab = load_8bit_int_array_from_file(DICTIONARY_PATH, &dict_size);
    // 0~65에 해당하는 index에 대해서 문자를 저장하는 배열 -> idx2char[0] = 'a' 이런식으로 사용
    char idx2char[VOCAB_SIZE];
    // 해당 배열의 문자에 대한 index를 저장하는 배열 -> char2idx['a'] = 0 이런식으로 사용, 얘는 0~255까지의 index를 가짐 
    // 실제로 사용하는 갯수는 65개
    char char2idx[255];
    make_dict(idx2char,char2idx,vocab);




    ////////////////////////필요한 가중치들 정리

    // input (batch_size, sequence_length) -> embedding (batch_size, sequence_length, embedding_dim)

    // lstm layer -> kernel(embedding_din , rnn_units), recurrent_kernel(rnn_units, rnn_units), bias(rnn_units,)

        // W_i, W_f, W_C, W_o => (embedding_dim, rnn_units)
        // U_i, U_f, U_C, U_o => (rnn_units, rnn_units)
        // b_i, b_f, b_C, b_o => (rnn_units,)

    // dense layer -> kernel(rnn_units, vocab_size), bias(vocab_size,)

    // output (batch_size, sequence_length, vocab_size)

    // 1. Embedding layer
    ////////////////////////embedding vector 
    // Load the weights of the Embedding layer
    size_t embedding_size;
    float *embedding = load_32bit_float_array_from_file(EMBEDDING_WEIGHTS_PATH,&embedding_size);
    float embedding_vector[VOCAB_SIZE][EMBEDDING_DIM];
    load_embedding_weights(embedding_vector,embedding);

    // 2. LSTM layer
    size_t kernel_size, recurrent_kernel_size, bias_size;
    float *kernel = load_32bit_float_array_from_file(LSTM_KERNEL_WEIGHTS_PATH,&kernel_size);
    float *recurrent_kernel = load_32bit_float_array_from_file(LSTM_RECURRENT_WEIGHTS_PATH,&recurrent_kernel_size);
    float *bias = load_32bit_float_array_from_file(LSTM_BIAS_PATH,&bias_size);

    float W_i[EMBEDDING_DIM][RNN_UNITS], W_f[EMBEDDING_DIM][RNN_UNITS], W_C[EMBEDDING_DIM][RNN_UNITS], W_o[EMBEDDING_DIM][RNN_UNITS];
    float U_i[RNN_UNITS][RNN_UNITS], U_f[RNN_UNITS][RNN_UNITS], U_C[RNN_UNITS][RNN_UNITS], U_o[RNN_UNITS][RNN_UNITS];
    float b_i[RNN_UNITS], b_f[RNN_UNITS], b_C[RNN_UNITS], b_o[RNN_UNITS];
    load_lstm_weights(W_i,W_f,W_C,W_o,U_i,U_f,U_C,U_o,b_i,b_f,b_C,b_o,kernel,recurrent_kernel,bias);


    // 3. Dense layer
    size_t Wout_size, bout_size;
    float *Wout = load_32bit_float_array_from_file(DENSE_WEIGHTS_PATH, &Wout_size);
    float *bout = load_32bit_float_array_from_file(DENSE_BIAS_PATH, &bout_size);
    float Dense_Wout[RNN_UNITS][VOCAB_SIZE];
    float Dense_bout[VOCAB_SIZE];
    load_dense_weights(Dense_Wout,Dense_bout,Wout,bout);
    

    // 4. Load the input text
    // char* input_text_string="ROMEO: ";
    const char start_text[2]="a";
    char input_text[2];
    input_text[0]='a';
    input_text[1]='\0';
    int input_text_len = strlen(input_text);

    int n=1;
    char generated_text[52];

    float h_t[BATCH_SIZE][RNN_UNITS];
    float c_t[BATCH_SIZE][RNN_UNITS];
    // 초기 상태 초기화
    for(int i=0;i<RNN_UNITS;i++){
        h_t[0][i] = 0;
        c_t[0][i] = 0;
    }

    for(int i=0;i<50;i++){
        // 5. Forward pass

        // 5.1. Embedding layer
        float x_t[BATCH_SIZE][input_text_len][EMBEDDING_DIM];
        for(int i=0;i<input_text_len;i++){
            for(int j=0;j<EMBEDDING_DIM;j++){
                x_t[0][i][j] = embedding_vector[char2idx[input_text[i]]][j];
            }
        }

        // 5.2. LSTM layer
        float lstm_output[BATCH_SIZE][SEQUENCE_LEN][RNN_UNITS];
        lstm_layer_forward(lstm_output,x_t,h_t,c_t,W_i,W_f,W_C,W_o,U_i,U_f,U_C,U_o,b_i,b_f,b_C,b_o);

        // 5.3. Dense layer
        float output[BATCH_SIZE][SEQUENCE_LEN][VOCAB_SIZE];
            // Dense 레이어 계산
        for (int b = 0; b < BATCH_SIZE; b++) {
            for (int s = 0; s < SEQUENCE_LEN; s++) {
                matmul(&output[b][s][0], &lstm_output[b][s][0], &Dense_Wout[0][0], 1, RNN_UNITS, VOCAB_SIZE);
                add_bias1(&output[b][s][0], Dense_bout, 1, VOCAB_SIZE);
            }
        } 
        
        // 결과 출력

        int max_idx=0;
        float max_val = output[0][0][0];

        for (int b = 0; b < BATCH_SIZE; b++) {
            for (int s = 0; s < SEQUENCE_LEN; s++) {
                for (int v = 0; v < VOCAB_SIZE; v++) {
                    if(output[b][s][v]>max_val){
                        max_val = output[b][s][v];
                        max_idx = v;
                    }
                }
            }
        }
  
        input_text[0]=idx2char[max_idx];   
        generated_text[n++]=idx2char[max_idx]; 
    }
    generated_text[0]=start_text[0];
    const char* filename = "./output.bin";
    // Open the binary file in write mode
    FILE *file = fopen(filename, "wb");
    if (file) {
        // Write the generated_text to the file
        fwrite(generated_text, sizeof(char), 51, file);
        fclose(file);
        printf("Successfully saved the generated text to the binary file.\n");
    } else {
        printf("Failed to open the binary file for writing.\n");
    }

    // 파일에서 문자열을 읽을 버퍼 할당
    char buffer[52];  // 버퍼 크기 설정

    // 파일에서 문자열 읽기
    read_string_from_binary_file(filename, buffer, sizeof(buffer));

    // 읽은 문자열 출력
    printf("%s\n", buffer);
    // Get the size of the binary file
    file = fopen(filename, "rb");
    if (file) {
        fseek(file, 0, SEEK_END);
        long size = ftell(file);
        fclose(file);
        printf("Binary file size: %ld bytes\n", size);
    } else {
        printf("Failed to open the binary file.\n");
    }


    return 0;
}

