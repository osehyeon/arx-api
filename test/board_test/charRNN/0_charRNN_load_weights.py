import tensorflow as tf
import numpy as np
import os

# 파일에서 32-bit 실수 배열을 불러옴
def load_32bit_float_array_from_file(filename):
    return np.fromfile(filename, dtype=np.float32)

# 파일에서 8-bit 부호 없는 정수 배열을 불러옴
def load_8bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

# 파일에 배열을 저장
def save_array_to_file(filename, data):
    data.tofile(filename)

# 양자화 파라미터 계산
def compute_quant_params(weight, axis=None):
    if axis is not None:
        # 축을 기준으로 최소/최대 값을 구합니다.
        min_val = weight.min(axis=axis, keepdims=True)
        max_val = weight.max(axis=axis, keepdims=True)
    else:
        # 전체 배열에 대한 최소/최대 값을 구합니다.
        min_val = weight.min()
        max_val = weight.max()

    # Scale과 zero_point를 계산합니다.
    scale = (max_val - min_val) / 255
    zero_point = np.round(-min_val / scale)

    # zero_point가 uint8 범위 내에 있는지 확인하고 조정합니다.
    zero_point = np.clip(zero_point, 0, 255)

    return scale.astype(np.float32), zero_point.astype(np.uint8)


def quantize(input, scale, zero_point):
    return np.clip(np.round(input / scale + zero_point), 0, 255).astype(np.uint8)

# 역양자화
def dequantize(input, scale, zero_point):
    return ((input.astype(np.int32) - np.int32(zero_point)) * scale).astype(np.float32)

# 양자화 파라미터 계산
def compute_quant_params(weight, axis=None):
    if axis is not None:
        # 축을 기준으로 최소/최대 값을 구합니다.
        min_val = weight.min(axis=axis, keepdims=True)
        max_val = weight.max(axis=axis, keepdims=True)
    else:
        # 전체 배열에 대한 최소/최대 값을 구합니다.
        min_val = weight.min()
        max_val = weight.max()

    # Scale과 zero_point를 계산합니다.
    scale = (max_val - min_val) / 255
    zero_point = np.round(-min_val / scale)

    # zero_point가 uint8 범위 내에 있는지 확인하고 조정합니다.
    zero_point = np.clip(zero_point, 0, 255)

    return scale.astype(np.float32), zero_point.astype(np.uint8)

MODEL_PATH='./charRNN.h5'
MODEL_NAME ='charRNN.h5'
# .h5 파일로부터 모델 로드
model = tf.keras.models.load_model('./charRNN.h5')
model.save('./charRNN.h5', save_format='tf')
model = tf.keras.models.load_model('./charRNN.h5')

# 가중치를 저장할 디렉토리 생성
weights_dir = 'weights'
os.makedirs(weights_dir, exist_ok=True)

# 모델의 각 레이어 가중치를 .bin 파일로 저장
for layer in model.layers:
    weights = layer.get_weights()  # 가중치 가져오기
    layer_name = layer.name

    for i, weight in enumerate(weights):
        # 가중치를 .bin 파일로 저장
        print(weight.shape) # 가중치 형태 출력
        weight_file = os.path.join(weights_dir, f"{layer_name}_weight_{i}.bin")
        weight.tofile(weight_file)
        print(f"Saved {layer_name} weight {i} to {weight_file}")

embedding_dim = 256
vocab_size = 65
rnn_units = 256