
import tensorflow as tf
import numpy as np
import os
from tqdm import tqdm
# 파일에 배열을 저장
def save_array_to_file(filename, data):
    data.tofile(filename)
# 1. 데이터 준비
url = 'https://storage.googleapis.com/download.tensorflow.org/data/shakespeare.txt'
path = tf.keras.utils.get_file('shakespeare.txt', url)
text = open(path, 'rb').read().decode(encoding='utf-8')


# 가중치를 저장할 디렉토리 생성
weights_dir = './text'
os.makedirs(weights_dir, exist_ok=True)


# 문자 집합 생성
vocab = sorted(set(text))
char2idx = {u: i for i, u in enumerate(vocab)}
idx2char = np.array(vocab)
vocab_size = len(vocab)
embedding_dim = 256
rnn_units = 1024
batch_size = 1  # 테스트할 때 배치 크기

idx2char_for_save = np.array([ord(c) for c in vocab],dtype=np.uint8)
save_array_to_file("./text/idx2char.bin",idx2char_for_save)

# 2. 모델 로드
model = tf.keras.models.load_model('./charRNN.h5')


text_generated = []

# input_array = np.expand_dims(input_array, 0)
start_string = "a"
input_eval=[char2idx[s] for s in start_string]
input_eval = tf.expand_dims(input_eval, 0)

embedding_layer = model.get_layer('embedding')
lstm_layer = model.get_layer('lstm')
dense_layer = model.get_layer('dense')

# predict 생성 함수
def predict_next_char(input_eval):
    embedding_vector = embedding_layer(input_eval)
    lstm_output = lstm_layer(embedding_vector)
    predictions = dense_layer(lstm_output)
    return predictions

print(input_eval)

    # tqdm을 사용하여 로딩 바 추가
for i in tqdm(range(50), desc="Generating text"):
    predictions = predict_next_char(input_eval)    
    
    predictions = tf.squeeze(predictions, 0)

    predicted_id = tf.argmax(predictions, axis=-1)[-1].numpy()

    input_eval = tf.expand_dims([predicted_id], 0)

    text_generated.append(idx2char[predicted_id])

gen_tx = start_string + ''.join(text_generated)

print(gen_tx)

array=np.array([ord(c) for c in gen_tx],dtype=np.uint8)
save_array_to_file("./text/golden_gen_text_by_layer.bin",array)

