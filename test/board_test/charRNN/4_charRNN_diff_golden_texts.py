import numpy as np
import os

class FileContentMismatchError(Exception):
    """Custom exception for file content mismatch."""
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)
        
def read_bin_file(filepath):
    """Read binary file and return its content as a numpy array."""
    with open(filepath, 'rb') as f:
        content = f.read()
        return content

def compare_bin_files(filepaths):
    """Compare the contents of multiple binary files."""
    # Read contents of all files
    contents = [read_bin_file(filepath) for filepath in filepaths]

    # Check if all files have the same content
    if all(content == contents[0] for content in contents):
        print("All files are identical.\n\n")
        
        # Output content as a string
        # We assume the content can be interpreted as a UTF-8 string, which may not always be the case
        try:
            content_string = contents[0].decode('utf-8')
            # print("File contents:\n", content_string)
        except UnicodeDecodeError:
            print("File contents are not valid UTF-8 strings and cannot be decoded.")
    else:
        raise FileContentMismatchError("Files have different contents.")

# Define the paths to the binary files
file1 = './text/golden_gen_text_by_layer.bin'
print("Size of file:", os.path.getsize(file1))
file2 = './text/golden_gen_text_by_manual.bin'
print("Size of file:", os.path.getsize(file2))
file3 = './text/golden_gen_text_by_model.bin'
print("Size of file:", os.path.getsize(file3))
# file4='./text/output.bin'
# print("Size of file:", os.path.getsize(file4))

# Compare the binary files
compare_bin_files([file1, file2, file3])
