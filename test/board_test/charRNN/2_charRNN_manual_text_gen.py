import tensorflow as tf
import numpy as np
import os

# 1. 데이터 준비
url = 'https://storage.googleapis.com/download.tensorflow.org/data/shakespeare.txt'
path = tf.keras.utils.get_file('shakespeare.txt', url)
text = open(path, 'rb').read().decode(encoding='utf-8')

# 가중치를 저장할 디렉토리 생성
weights_dir = './text'
os.makedirs(weights_dir, exist_ok=True)

# 문자 집합 생성
vocab = sorted(set(text))
char2idx = {u: i for i, u in enumerate(vocab)}
idx2char = np.array(vocab)
vocab_size = len(vocab)
embedding_dim = 256
rnn_units = 256
batch_size = 1  # 테스트할 때 배치 크기


def save_array_to_file(filename, data):
    data.tofile(filename)
    
def load_dense_weight():
    dense_weight = load_32bit_float_array_from_file('./weights/dense_weight_0.bin')
    dense_weight = dense_weight.reshape((rnn_units, 65))
    return dense_weight

def load_dense_bias():
    dense_bias = load_32bit_float_array_from_file('./weights/dense_weight_1.bin')
    return dense_bias

def load_32bit_float_array_from_file(filename):
    return np.fromfile(filename, dtype=np.float32)

# 파일에서 8-bit 부호 없는 정수 배열을 불러옴
def load_8bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

def load_lstm_kernel_weight():
    kernel = load_32bit_float_array_from_file('./weights/lstm_weight_0.bin')
    kernel = kernel.reshape((embedding_dim, rnn_units*4))
    return kernel

def load_lstm_recurrent_kernel_weight():
    recurrent_kernel = load_32bit_float_array_from_file('./weights/lstm_weight_1.bin')
    recurrent_kernel = recurrent_kernel.reshape((embedding_dim, rnn_units*4))
    return recurrent_kernel

def load_lstm_bias():
    bias = load_32bit_float_array_from_file('./weights/lstm_weight_2.bin')
    return bias

def load_lstm_weight():
    kernel = load_32bit_float_array_from_file('./weights/lstm_weight_0.bin')
    kernel = kernel.reshape((embedding_dim, rnn_units*4))
    recurrent_kernel = load_32bit_float_array_from_file('./weights/lstm_weight_1.bin')
    recurrent_kernel = recurrent_kernel.reshape((rnn_units, rnn_units*4))
    bias = load_32bit_float_array_from_file('./weights/lstm_weight_2.bin')
    return tf.convert_to_tensor(kernel), tf.convert_to_tensor(recurrent_kernel), tf.convert_to_tensor(bias)
def tf_sigmoid(x):
    return tf.math.sigmoid(x)

def tf_tanh(x):
    return tf.math.tanh(x)
# LSTM 게이트 계산
def compute_lstm_output(x_t, h_prev, C_prev, W_i, W_f, W_C, W_o, U_i, U_f, U_C, U_o, b_i, b_f, b_C, b_o):
    # 각 게이트의 계산
    # TensorFlow 텐서로 변환
    batch_size, sequence_length , embedding_dim= x_t.shape
    

    x_t_tf = tf.convert_to_tensor(x_t, dtype=tf.float32) # (batch_size, seq_length, embedding_dim)
    # print(x_t_tf.shape)
    x_t = np.squeeze(x_t,0)
    h_prev_tf = tf.convert_to_tensor(h_prev, dtype=tf.float32)
    C_prev_tf = tf.convert_to_tensor(C_prev, dtype=tf.float32)
    h_list = []

    
    # Gates computations
    for t in range(sequence_length):
        x_t_tf = tf.expand_dims(x_t[t], axis=0)
        # print(x_t_tf.shape)
        #input gate
        i_t = tf_sigmoid(tf.matmul(x_t_tf, W_i) + tf.matmul(h_prev_tf, U_i) + b_i)
        #forget gate
        f_t = tf_sigmoid(tf.matmul(x_t_tf, W_f) + tf.matmul(h_prev_tf, U_f) + b_f)
        #candidate cell state
        C_tilde = tf_tanh(tf.matmul(x_t_tf, W_C) + tf.matmul(h_prev_tf, U_C) + b_C)
        #cell state 
        C_t = f_t * C_prev_tf + i_t * C_tilde
        #output gate
        o_t = tf_sigmoid(tf.matmul(x_t_tf, W_o) + tf.matmul(h_prev_tf, U_o) + b_o)
        #hidden state
        h_t = o_t * tf_tanh(C_t)
        h_prev_tf = h_t
        C_prev_tf = C_t
        h_list.append(h_t)
    # print(h_list)
    h_all_steps = tf.stack(h_list, axis=1)
    return h_all_steps, h_t, C_t

def lstm_forward(input_eval, h_prev, C_prev):
    embedding_weight = load_32bit_float_array_from_file('./weights/embedding_weight_0.bin')
    embedding_weight = embedding_weight.reshape((vocab_size, embedding_dim))
    embedding_vector = []   
    for i in input_eval:
        embedding_vector.append(embedding_weight[i])
    embedding_vector = np.array(embedding_vector)
    
    x_t = embedding_vector
    
    kernel, recurrent_kernel, bias = load_lstm_weight()
    W_i, W_f, W_C, W_o = np.split(kernel, 4, axis=1)
    U_i, U_f, U_C, U_o = np.split(recurrent_kernel, 4, axis=1)
    b_i, b_f, b_C, b_o = np.split(bias, 4)
    h_t_all_steps, h_prev, C_prev = compute_lstm_output(
        x_t, h_prev, C_prev,
        W_i, W_f, W_C, W_o,
        U_i, U_f, U_C, U_o,
        b_i, b_f, b_C, b_o
    )
    W_dense=load_dense_weight()
    b_dense=load_dense_bias()
    output = np.dot(h_t_all_steps, W_dense) + b_dense
    return output, h_prev, C_prev

def generate_text(start_string):
    num_generate = 50  # 생성할 문자의 수

    input_eval = [char2idx[s] for s in start_string] # 시작 문자열을 숫자로 변환
    input_eval = tf.expand_dims(input_eval, 0) # 배치 차원 추가
    
    h_prev=np.zeros((batch_size, rnn_units))
    C_prev=np.zeros((batch_size, rnn_units))

    text_generated = []


    # tqdm을 사용하여 로딩 바 추가
    for i in range(num_generate):
        predictions, h_prev, C_prev = lstm_forward(input_eval,h_prev, C_prev)
        # print(predictions)
        # print(predictions.shape)
        predictions = tf.squeeze(predictions, 0)

        predicted_id = tf.argmax(predictions, axis=-1)[-1].numpy()

        input_eval = tf.expand_dims([predicted_id], 0)

        text_generated.append(idx2char[predicted_id])

    return (start_string + ''.join(text_generated))

gen_tx=generate_text("a")
# print(gen_tx)
array=np.array([ord(c) for c in gen_tx],dtype=np.uint8)
save_array_to_file("./text/golden_gen_text_by_manual.bin",array)
file_size = os.path.getsize("./text/golden_gen_text_by_manual.bin")
# print("File size:", file_size, "bytes")