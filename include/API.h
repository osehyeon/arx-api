#include <stdbool.h>

/**
 * Performs convolution operation on the input tensor using the given kernel and stores the result in the output tensor after right shift.
 * @param input Pointer to the input tensor.
 * @param kernel Pointer to the kernel tensor.
 * @param bias Pointer to the bias tensor.
 * @param output Pointer to the output tensor.
 * @param N Number of input images.
 * @param H Height of input images.
 * @param W Width of input images.
 * @param C Number of input channels.
 * @param KN Number of output channels.
 * @param KH Height of kernel.
 * @param KW Width of kernel.
 * @param pad_size Padding size.
 * @param stride_size Stride size.
 * @param doRelu Boolean value indicating whether to apply ReLU activation function.
 * @param doBias Boolean value indicating whether to add bias.
 * @param shift Shift value.
 * @param out_h Height of output tensor.
 * @param out_w Width of output tensor.
 * @return Integer value indicating the status of the operation.
 */
int convolution_i8(unsigned char* input, unsigned char* kernel, unsigned char* bias, unsigned char* output,
                   unsigned char N, unsigned char H, unsigned char W, unsigned char C, unsigned char KN, unsigned char KH, unsigned char KW,
                   unsigned char pad_size, unsigned char stride_size, bool doRelu, bool doBias, unsigned char out_h, unsigned char out_w);

/**
 * Performs convolution operation on the input tensor using the given kernel and stores the result in the output tensor after right shift.
 * @param input Pointer to the input tensor.
 * @param kernel Pointer to the kernel tensor.
 * @param bias Pointer to the bias tensor.
 * @param output Pointer to the output tensor.
 * @param N Number of input images.
 * @param H Height of input images.
 * @param W Width of input images.
 * @param C Number of input channels.
 * @param KN Number of output channels.
 * @param KH Height of kernel.
 * @param KW Width of kernel.
 * @param pad_size Padding size.
 * @param stride_size Stride size.
 * @param doRelu Boolean value indicating whether to apply ReLU activation function.
 * @param doBias Boolean value indicating whether to add bias.
 * @param shift Shift value.
 * @param out_h Height of output tensor.
 * @param out_w Width of output tensor.
 * @return Integer value indicating the status of the operation.
 */
int convolution_i8_shift(unsigned char* input, unsigned char* kernel, unsigned char *bias, unsigned char* output,
                         unsigned char N, unsigned char H, unsigned char W, unsigned char C, unsigned char KN, unsigned char KH, unsigned char KW,
                         unsigned char pad_size, unsigned char stride_size, bool doRelu, bool doBias, unsigned char shift, unsigned char out_h, unsigned char out_w);


/**
 * Performs a convolution operation on the input tensor using the provided kernel, bias, and other parameters.
 * All tensors are integer tensors with 8-bit elements. They are scaled and offset by the provided scaling and offset parameters.
 *
 * @param input Pointer to the input tensor data.
 * @param inputScale Scaling factor for the input tensor data.
 * @param inputOffset Offset for the input tensor data.
 * @param kernel Pointer to the kernel data.
 * @param kernelScale Scaling factor for the kernel data.
 * @param kernelOffset Offset for the kernel data.
 * @param bias Pointer to the bias data.
 * @param biasScale Scaling factor for the bias data.
 * @param biasOffset Offset for the bias data.
 * @param output Pointer to the output tensor data.
 * @param outputScale Scaling factor for the output tensor data.
 * @param outputOffset Offset for the output tensor data.
 * @param N Number of input images.
 * @param H Height of the input images.
 * @param W Width of the input images.
 * @param C Number of channels in the input images.
 * @param KN Number of filters in the kernel.
 * @param KH Height of the kernel.
 * @param KW Width of the kernel.
 * @param pad_size Padding size.
 * @param stride_size Stride size.
 * @param doRelu Whether to apply ReLU activation function to the output.
 * @param doBias Whether to add bias to the output.
 * @param out_h Height of the output tensor.
 * @param out_w Width of the output tensor.
 * @return An integer value indicating the success or failure of the operation.
 */
int convolution_i8_scale(unsigned char* input, float *inputScale, unsigned char *inputOffset,
                        unsigned char* kernel, float *kernelScale, unsigned char *kernelOffset,
                        unsigned char* bias, float *biasScale, unsigned char *biasOffset,
                        unsigned char* output, float* outputScale, unsigned char* outputOffset,
                        unsigned char N, unsigned char H, unsigned char W, unsigned char C,
                        unsigned char KN, unsigned char KH, unsigned char KW,
                        unsigned char pad_size, unsigned char stride_size, bool doRelu, bool doBias,
                        unsigned char out_h, unsigned char out_w);


/**
 * Performs a 2D convolution operation on the input tensor using the provided kernel and bias tensors.
 *
 * @param input Pointer to the input tensor data.
 * @param kernel Pointer to the kernel tensor data.
 * @param bias Pointer to the bias tensor data.
 * @param output Pointer to the output tensor data.
 * @param N Number of input tensors in the batch.
 * @param H Height of the input tensor.
 * @param W Width of the input tensor.
 * @param C Number of channels in the input tensor.
 * @param KN Number of kernels in the kernel tensor.
 * @param KH Height of each kernel in the kernel tensor.
 * @param KW Width of each kernel in the kernel tensor.
 * @param pad_size Size of the padding to be applied to the input tensor.
 * @param stride_size Stride size to be used for the convolution operation.
 * @param doRelu Whether or not to apply ReLU activation to the output tensor.
 * @param doBias Whether or not to add the bias tensor to the output tensor.
 * @param out_h Height of the output tensor.
 * @param out_w Width of the output tensor.
 * @return An integer indicating the success (0) or failure (-1) of the operation.
 */
int convolution_fp32(float* input, float *kernel, float *bias, float *output, 
                    unsigned char N, unsigned char H, unsigned char W, unsigned char C,
                    unsigned char KN, unsigned char KH, unsigned char KW, 
                    int pad_size, int stride_size, bool doRelu, bool doBias, 
                    unsigned char out_h, unsigned char out_w);


/**
 * @brief Performs a fully connected operation on the input tensor using int8 data type with quantization parameters for input, kernel, bias, and output.
 * 
 * @param input Pointer to the input tensor data.
 * @param inputScale Quantization scale for the input tensor.
 * @param inputOffset Quantization offset for the input tensor.
 * @param kernel Pointer to the kernel tensor data.
 * @param kernelScale Quantization scale for the kernel tensor.
 * @param kernelOffset Quantization offset for the kernel tensor.
 * @param bias Pointer to the bias tensor data.
 * @param biasScale Quantization scale for the bias tensor.
 * @param biasOffset Quantization offset for the bias tensor.
 * @param output Pointer to the output tensor data.
 * @param outputScale Quantization scale for the output tensor.
 * @param outputOffset Quantization offset for the output tensor.
 * @param inputDim0 First dimension of the input tensor.
 * @param inputDim1 Second dimension of the input tensor.
 * @param kernelDim0 First dimension of the kernel tensor.
 * @param kernelDim1 Second dimension of the kernel tensor.
 * @param doBias Boolean flag indicating whether to add bias to the output tensor.
 * @param outputDim0 First dimension of the output tensor.
 * @param outputDim1 Second dimension of the output tensor.
 * @return int Returns 0 if the operation is successful, otherwise returns an error code.
 */
int fullyconnected_i8_scale(unsigned char *input, float *inputScale, unsigned char *inputOffset,
                           unsigned char *kernel, float *kernelScale, unsigned char *kernelOffset,
                           unsigned char *bias, float *biasScale, unsigned char *biasOffset,
                           unsigned char *output, float *outputScale, unsigned char *outputOffset,
                           unsigned int inputDim0, unsigned int inputDim1, 
                           unsigned int kernelDim0, unsigned int kernelDim1,
                           bool doBias, unsigned int outputDim0, unsigned int outputDim1);



/**
 * Computes the fully connected layer with 8-bit integer inputs and weights, with optional bias and output shifting.
 *
 * @param input Pointer to the input tensor data.
 * @param kernel Pointer to the kernel (weight) tensor data.
 * @param bias Pointer to the bias tensor data. If doBias is false, this parameter is ignored.
 * @param output Pointer to the output tensor data.
 * @param inputDim0 The first dimension of the input tensor.
 * @param inputDim1 The second dimension of the input tensor.
 * @param kernelDim0 The first dimension of the kernel tensor.
 * @param kernelDim1 The second dimension of the kernel tensor.
 * @param doBias Whether to add bias to the output or not.
 * @param shift The amount of right shift applied to the output. If shift is 0, no shift is applied.
 * @param outputDim0 The first dimension of the output tensor.
 * @param outputDim1 The second dimension of the output tensor.
 *
 * @return The number of cycles taken to compute the fully connected layer.
 */
int fullyconnected_i8_shift(unsigned char* input, unsigned char *kernel, unsigned char *bias, unsigned char *output,
                            unsigned char inputDim0, unsigned char inputDim1, unsigned char kernelDim0, unsigned char kernelDim1,
                            bool doBias, unsigned char shift, unsigned char outputDim0, unsigned char outputDim1);


/**
 * Applies max pooling to the input tensor and stores the result in the output tensor.
 *
 * @param input Pointer to the input tensor.
 * @param output Pointer to the output tensor.
 * @param N Number of input tensors.
 * @param H Height of the input tensor.
 * @param W Width of the input tensor.
 * @param C Number of channels in the input tensor.
 * @param KH Height of the kernel.
 * @param KW Width of the kernel.
 * @param pad_size Padding size.
 * @param stride_size Stride size.
 * @return 0 if successful, non-zero otherwise.
 */
int maxpool_i8(unsigned char* input, unsigned char *output, 
            unsigned char N, unsigned char H, unsigned char W, unsigned char C, unsigned char KH, unsigned char KW,
            unsigned char pad_size, unsigned char stride_size);


/**
 * Computes the average pooling of an input tensor with 8-bit unsigned integer elements.
 *
 * @param input Pointer to the input tensor data.
 * @param inputScale The scaling factor for the input tensor.
 * @param inputOffset The offset for the input tensor.
 * @param output Pointer to the output tensor data.
 * @param outputScale The scaling factor for the output tensor.
 * @param outputOffset The offset for the output tensor.
 * @param N The batch size of the input tensor.
 * @param H The height of the input tensor.
 * @param W The width of the input tensor.
 * @param C The number of channels in the input tensor.
 * @param KH The height of the pooling kernel.
 * @param KW The width of the pooling kernel.
 * @param pad_size The size of the padding applied to the input tensor.
 * @param stride_size The stride used for the pooling operation.
 * @return An integer indicating the success (0) or failure (non-zero) of the operation.
 */
int avgpool_i8(unsigned char* input, float *inputScale, unsigned char *inputOffset, 
            unsigned char *output, float *outputScale, unsigned char *outputOffset,
            unsigned char N, unsigned char H, unsigned char W, unsigned char C, 
            unsigned char KH, unsigned char KW, unsigned char pad_size, unsigned char stride_size);


/**
 * Applies the Rectified Linear Unit (ReLU) activation function to the input array.
 * ReLU sets all negative values in the input array to zero, and leaves all non-negative values unchanged.
 *
 * @param input Pointer to the input array.
 * @param output Pointer to the output array.
 * @param size The size of the input and output arrays.
 * @return The number of elements in the output array that were set to zero.
 */
int relu(unsigned char* input, unsigned char *output, unsigned long size, unsigned char *offset);


/**
 * Quantizes the input array of size `size` with the given `scale` and `offset`,
 * and stores the result in the output array.
 *
 * @param input Pointer to the input array.
 * @param output Pointer to the output array.
 * @param size The size of the input and output arrays.
 * @param scale The scale factor used for quantization.
 * @param offset The offset used for quantization.
 * @return The number of elements quantized.
 */
int quantize(float *input, unsigned char *output, unsigned long size, float *scale, unsigned char *offset);


/**
 * @brief Dequantizes the input tensor using the given scale and offset.
 *
 * @param input Pointer to the input tensor.
 * @param output Pointer to the output tensor.
 * @param size Number of elements in the input tensor.
 * @param scale Scale factor used for dequantization.
 * @param offset Offset used for dequantization.
 * @return 0 if successful, non-zero otherwise.
 */
int dequantize(const unsigned char *input, float *output, unsigned long size, float *scale, unsigned char *zero_point, int channel_size, bool use_stride);


/**
 * Transposes a 4D array of 8-bit unsigned integers.
 *
 * @param input Pointer to the input array.
 * @param output Pointer to the output array.
 * @param inDim0 The size of the first dimension of the input array.
 * @param inDim1 The size of the second dimension of the input array.
 * @param inDim2 The size of the third dimension of the input array.
 * @param inDim3 The size of the fourth dimension of the input array.
 * @param outDim0 The size of the first dimension of the output array.
 * @param outDim1 The size of the second dimension of the output array.
 * @param outDim2 The size of the third dimension of the output array.
 * @param outDim3 The size of the fourth dimension of the output array.
 * @param inx0 The index of the first dimension of the input array to transpose.
 * @param idx1 The index of the second dimension of the input array to transpose.
 * @param idx2 The index of the third dimension of the input array to transpose.
 * @param idx3 The index of the fourth dimension of the input array to transpose.
 * @return 0 if successful, -1 otherwise.
 */
int transpose_i8(unsigned char *input, unsigned char *output,
                 unsigned long inDim0, unsigned long inDim1, unsigned long inDim2, unsigned long inDim3,
                 unsigned long outDim0, unsigned long outDim1, unsigned long outDim2, unsigned long outDim3,
                 unsigned char idx0, unsigned char idx1, unsigned char idx2, unsigned char idx3);


/**
 * @brief Adds two arrays of elements and stores the result in another array.
 * 
 * This function adds two arrays of elements, scales the result with destScale, and offsets it with destOffset.
 * The input arrays are scaled with input0Scale and input1Scale, and offset with input0Offset and input1Offset, respectively.
 * The size parameter specifies the number of elements in the arrays.
 * 
 * @param input0 Pointer to the first input array.
 * @param input0Scale Scaling factor for the first input array.
 * @param input0Offset Offset for the first input array.
 * @param input1 Pointer to the second input array.
 * @param input1Scale Scaling factor for the second input array.
 * @param input1Offset Offset for the second input array.
 * @param output Pointer to the output array.
 * @param destScale Scaling factor for the output array.
 * @param destOffset Offset for the output array.
 * @param size Number of elements in the arrays.
 * @return int Returns 0 if successful, otherwise returns an error code.
 */
int elemadd_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
               unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size);


/**
 * Adds two arrays element-wise and applies the ReLU activation function to the result.
 *
 * @param input0 Pointer to the first input array.
 * @param input0Scale Scale factor for the first input array.
 * @param input0Offset Offset for the first input array.
 * @param input1 Pointer to the second input array.
 * @param input1Scale Scale factor for the second input array.
 * @param input1Offset Offset for the second input array.
 * @param output Pointer to the output array.
 * @param destScale Scale factor for the output array.
 * @param destOffset Offset for the output array.
 * @param size Number of elements in the arrays.
 * @return 0 if successful, non-zero otherwise.
 */
int elemadd_relu_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
                    unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
                    unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size);


/**
 * Subtracts two arrays element-wise and writes the result to a third array.
 *
 * @param input0 Pointer to the first input array.
 * @param input0Scale Scale factor for the first input array.
 * @param input0Offset Offset for the first input array.
 * @param input1 Pointer to the second input array.
 * @param input1Scale Scale factor for the second input array.
 * @param input1Offset Offset for the second input array.
 * @param output Pointer to the output array.
 * @param destScale Scale factor for the output array.
 * @param destOffset Offset for the output array.
 * @param size Number of elements in the arrays.
 * @return The number of elements written to the output array.
 */
int elemsub_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
            unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size);


/**
 * Performs element-wise division of two input arrays and stores the result in the output array.
 *
 * @param input0 Pointer to the first input array.
 * @param input0Scale Scale factor for the first input array.
 * @param input0Offset Offset for the first input array.
 * @param input1 Pointer to the second input array.
 * @param input1Scale Scale factor for the second input array.
 * @param input1Offset Offset for the second input array.
 * @param output Pointer to the output array.
 * @param destScale Scale factor for the output array.
 * @param destOffset Offset for the output array.
 * @param size Number of elements in the input and output arrays.
 * @return 0 if successful, non-zero otherwise.
 */
int elemdiv_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
            unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size);


/**
 * Computes the element-wise maximum of two input arrays and stores the result in the output array.
 *
 * @param input0 Pointer to the first input array.
 * @param input0Scale Scale factor for the first input array.
 * @param input0Offset Offset for the first input array.
 * @param input1 Pointer to the second input array.
 * @param input1Scale Scale factor for the second input array.
 * @param input1Offset Offset for the second input array.
 * @param output Pointer to the output array.
 * @param destScale Scale factor for the output array.
 * @param destOffset Offset for the output array.
 * @param size Number of elements in the input and output arrays.
 * @return The number of elements written to the output array.
 */
int elemmax_i8(unsigned char *input0, float *input0Scale, unsigned char *input0Offset,
            unsigned char *input1, float *input1Scale, unsigned char *input1Offset,
            unsigned char *output, float *destScale, unsigned char *destOffset, unsigned long size);


/**
 * Fills a buffer with a given value.
 *
 * @param input Pointer to the buffer to fill.
 * @param size The size of the buffer to fill.
 * @param value The value to fill the buffer with.
 * @return The number of bytes written to the buffer.
 */
int splat_i8(unsigned char *input, unsigned long size, unsigned char value);


/**
 * Applies the softmax function to the input tensor along the last dimension.
 *
 * @param input Pointer to the input tensor.
 * @param output Pointer to the output tensor.
 * @param inDim0 The size of the first dimension of the input tensor.
 * @param inDim1 The size of the second dimension of the input tensor.
 * @param outDim0 The size of the first dimension of the output tensor.
 * @param outDim1 The size of the second dimension of the output tensor.
 * @return 0 if successful, non-zero otherwise.
 */
int softmax(unsigned char *input, float *output, unsigned long inDim0, unsigned long inDim1, unsigned long outDim0, unsigned long outDim1);


/**
 * Applies the hyperbolic tangent (tanh) activation function to the input array.
 * The input array is assumed to be an array of 8-bit unsigned integers.
 * The output array is also an array of 32-bit floats.
 * The offset parameter is used to adjust the output of the tanh function.
 * 
 * @param input Pointer to the input array.
 * @param output Pointer to the output array.
 * @param offset Pointer to the offset array.
 * @param size The size of the input and output arrays.
 * @return The number of elements in the output array that were set to zero.
 */
int tanh_activation(unsigned char *input, float *output, unsigned long size,float *scale, unsigned char* offset);

/**
 * Applies the hyperbolic tangent (tanh) activation function to the input array.
 * The input array is assumed to be an array of 8-bit unsigned integers.
 * The output array is also an array of 8-bit unsigned integers.
 * The offset parameter is used to adjust the output of the tanh function.
 * @param input Pointer to the input array.
 * @param output Pointer to the output array.
 * @param size The size of the input and output arrays.
 * @param scale Pointer to the scale array.
 * @param offset Pointer to the offset array.
 * @return The number of elements in the output array that were set to zero.
 */
int tanh_activation_i8(unsigned char *input, unsigned char *output, unsigned long size, float *scale, unsigned char* offset);